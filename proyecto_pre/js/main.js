import '../css/main.scss';
import '@babel/polyfill';

import { geoConicConformalSpain } from 'd3-composite-projections';
import { feature } from 'topojson-client';

//Variables a utilizar por los tres mapas
let mapWidth = document.getElementById('map_municipales').clientWidth, mapHeight = document.getElementById('map_municipales').clientHeight;
let poligonos, poligonosFeatures, path, projection, auxFeatures_v2;

//Otros
const radius = window.innerWidth < 620 ? 45 : 55, maxArea = Math.PI * Math.pow(radius,2);
const padding = 2;

initData();

function initData() {
    let q = d3.queue();

    q.defer(d3.json, 'https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/batalla_izquierda/municipios_with_population_179_v2.json');
    q.defer(d3.csv, 'https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/batalla_izquierda/elecciones_juntos.csv');

    q.await(function(error, map, data) {
        if (error) throw error;

        poligonos = feature(map, map.objects.municipios);
        projection = geoConicConformalSpain().scale(2000).fitSize([mapWidth,mapHeight], poligonos);
        poligonosFeatures = [...poligonos.features];        
        path = d3.geoPath(projection);

        poligonosFeatures.forEach(function(item) {
            item.centroid = path.centroid(item);
            let innerData = data.filter(function(subItem) { if (item.properties.COD == subItem.COD) { return subItem; }})[0];
            item.properties.nombre = innerData.Municipio;
            item.properties.ganador_municipales = innerData.Municipales;
            item.properties.ganador_autonomicas = innerData.Autonómicas;
            item.properties.ganador_generales =  innerData.Generales;
        });

        let maxData = d3.max(poligonosFeatures, function(d) { return +d.properties.POBLACION_MUNI;});

        let auxFeatures = [...poligonosFeatures];
        auxFeatures.forEach(function(d) {
            d.radius = setRadius(d);
        });

        auxFeatures_v2 = applyForce(auxFeatures);

        function applyForce(nodes) {
            const simulation = d3.forceSimulation(nodes)
                .force("cx", d3.forceX().x(d => mapWidth / 2).strength(0.02))
                .force("cy", d3.forceY().y(d => mapWidth * (5/8) / 2).strength(0.02))
                .force("x", d3.forceX().x(d => d.centroid ? d.centroid[0] : 0).strength(1))
                .force("y", d3.forceY().y(d => d.centroid ? d.centroid[1] : 0).strength(1))
                .force("charge", d3.forceManyBody().strength(-1))
                .force("collide", d3.forceCollide().radius(d => d.radius + 0.15).strength(1.5))
                .stop();
            
            let i = 0; 
            while (simulation.alpha() > 0.01 && i < 200) {
                simulation.tick(); 
                i++;
            }     
            
            return simulation.nodes();
        }

        function setRadius(d){
            let proporcion = 17 + ((+d.properties.POBLACION_MUNI * maxArea) / maxData);
            return Math.sqrt(proporcion / Math.PI);
        }

        //Desarrollo de cada mapa
        initMunicipales();
        initAutonomicas();
        initGenerales();
    });
}

function initMunicipales() {
    let svg = d3.select('#map_municipales').append("svg")
        .attr("width", mapWidth)
        .attr("height", mapHeight);

    let g = svg
        .append('g');

    g.selectAll("circle")
        .data(auxFeatures_v2)
        .enter()
        .append("circle")
        .attr("cx", d => d.x)
        .attr("cy", d => d.y)
        .attr("r", d => d.radius)
        .attr('fill', function(d) {
            if(d.properties.ganador_municipales == 'PSOE') {
                return '#ea5458';
            } else if (d.properties.ganador_municipales == 'MÁS MADRID') {
                return '#2adcc4';
            } else {
                return '#693067';
            }
        })
        .attr('stroke', 'white');    
}

function initAutonomicas() {
    let svg = d3.select('#map_autonomicas').append("svg")
        .attr("width", mapWidth)
        .attr("height", mapHeight);

    let g = svg
        .append('g');

    g.selectAll("circle")
        .data(auxFeatures_v2)
        .enter()
        .append("circle")
        .attr("cx", d => d.x)
        .attr("cy", d => d.y)
        .attr("r", d => d.radius)
        .attr('fill', function(d) {
            if(d.properties.ganador_autonomicas == 'PSOE') {
                return '#ea5458';
            } else if (d.properties.ganador_autonomicas == 'MÁS MADRID') {
                return '#2adcc4';
            } else {
                return '#693067';
            }
        })
        .attr('stroke', 'white'); 
}

function initGenerales() {
    let svg = d3.select('#map_generales').append("svg")
        .attr("width", mapWidth)
        .attr("height", mapHeight);

    let g = svg
        .append('g');

    g.selectAll("circle")
        .data(auxFeatures_v2)
        .enter()
        .append("circle")
        .attr("cx", d => d.x)
        .attr("cy", d => d.y)
        .attr("r", d => d.radius)
        .attr('fill', function(d) {
            if(d.properties.ganador_generales == 'PSOE') {
                return '#ea5458';
            } else if (d.properties.ganador_generales == 'MÁS MADRID') {
                return '#2adcc4';
            } else {
                return '#693067';
            }
        })
        .attr('stroke', 'white'); 
}